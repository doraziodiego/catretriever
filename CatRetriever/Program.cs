﻿using System;

namespace CatRetriever
{
    class Program
    {
        static void Main()
        {
            var owners = new CatServiceClient().GetOwners();
            var catGroups = new CatGrouper(owners).CatGroups;
            foreach (var catGroup in catGroups)
            {
                Console.WriteLine(catGroup);
            }
            Console.ReadKey();
        }
    }
}
