﻿using System.Linq;
using CatRetriever;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CatRetrieverTest
{
    [TestClass]
    public class CatGrouperTest
    {
        [TestMethod]
        public void TestCatGrouper()
        {
            var owners = new[]
            {
                new Owner {Gender = "Male", Cats = new[] {new Cat {Name = "Simba"}}},
                new Owner {Gender = "Female", Cats = new[] {new Cat {Name = "Nemo"}}},
                new Owner {Gender = "Male", Cats = new[] {new Cat {Name = "Garfield"}, new Cat {Name = "Felix"}}}
            };

            var catGroups = new CatGrouper(owners).CatGroups.OrderBy(g => g.OwnersGender).ToArray();

            Assert.AreEqual(2, catGroups.Length);

            Assert.AreEqual("Female", catGroups[0].OwnersGender);
            Assert.AreEqual("Nemo", catGroups[0].Cats[0].Name);

            Assert.AreEqual("Male", catGroups[1].OwnersGender);
            Assert.AreEqual("Felix", catGroups[1].Cats[0].Name);
            Assert.AreEqual("Garfield", catGroups[1].Cats[1].Name);
            Assert.AreEqual("Simba", catGroups[1].Cats[2].Name);
        }
    }
}
