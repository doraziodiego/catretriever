﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace CatRetriever
{
    public class CatServiceClient
    {
        public const string ServiceUrl = "http://agl-developer-test.azurewebsites.net/people.json";

        public Owner[] GetOwners()
        {
            try
            {
                var response = new HttpClient().Get(ServiceUrl);
                if (response == null)
                {
                    throw new InvalidOperationException("CatServiceClient.GetOwners failed");
                }
                dynamic jsonObject = JsonConvert.DeserializeObject(response);
                return ParseResult(jsonObject);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        protected Owner[] ParseResult(dynamic jsonObject)
        {
            var owners = new List<Owner>();
            foreach (var person in jsonObject)
            {
                var owner = new Owner
                {
                    Gender = person.gender,
                    Cats = new Cat[0]
                };
                if (person.pets != null)
                {
                    var cats = new List<Cat>();
                    foreach (var cat in person.pets)
                    {
                        if (cat.type == "Cat")
                        {
                            cats.Add(new Cat { Name = cat.name });
                        }
                    }
                    owner.Cats = cats.ToArray();
                }
                owners.Add(owner);
            }
            return owners.ToArray();
        }
    }
}
