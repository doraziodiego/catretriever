﻿using System.Text;

namespace CatRetriever
{
    public class CatGroup
    {
        public string OwnersGender { get; set; }

        public Cat[] Cats { get; set; }

        public override string ToString()
        {
            var result = new StringBuilder();
            result.AppendLine(OwnersGender);
            foreach (var cat in Cats)
            {
                result.AppendLine($"- {cat.Name}");
            }
            return result.ToString();
        }
    }
}
