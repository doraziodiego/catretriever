﻿using System.Linq;

namespace CatRetriever
{
    public class CatGrouper
    {
        public readonly CatGroup[] CatGroups;

        public CatGrouper(Owner[] owners)
        {
            CatGroups = owners
                .GroupBy(owner => owner.Gender)
                .Select(g => new CatGroup
                {
                    OwnersGender = g.Key,
                    Cats = g
                        .SelectMany(o => o.Cats)
                        .OrderBy(o => o.Name)
                        .ToArray()
                })
                .ToArray();
        }
    }
}
