﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;

namespace CatRetriever
{
    public class HttpClient
    {
        public string Get(string uri)
        {
            try
            {
                var request = WebRequest.Create(uri);
                using (var response = request.GetResponse())
                {
                    using (var responseStream = response.GetResponseStream())
                    {
                        using (var reader = new StreamReader(responseStream))
                        {
                            return reader.ReadToEnd();
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                Debug.WriteLine($"HttpClient.Get(\"{uri}\") - exception thrown:{Environment.NewLine}{exception}");
                return null;
            }
        }
    }
}
