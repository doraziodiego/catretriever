﻿using CatRetriever;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json.Linq;

namespace CatRetrieverTest
{
    [TestClass]
    public class CatServiceClientTest
    {
        public class CatServiceClientForTesting : CatServiceClient
        {
            public Owner[] ParseResultPublic(dynamic jsonObject)
            {
                return ParseResult(jsonObject);
            }
        }

        [TestMethod]
        public void TestParseReult()
        {
            const string jsonString =
                "[{\"name\":\"Bob\",\"gender\":\"Male\",\"age\":23,\"pets\":[{\"name\":\"Garfield\",\"type\":\"Cat\"},{\"name\":\"Fido\",\"type\":\"Dog\"}]},{\"name\":\"Jennifer\",\"gender\":\"Female\",\"age\":18,\"pets\":[{\"name\":\"Garfield\",\"type\":\"Cat\"}]},{\"name\":\"Steve\",\"gender\":\"Male\",\"age\":45,\"pets\":null},{\"name\":\"Fred\",\"gender\":\"Male\",\"age\":40,\"pets\":[{\"name\":\"Tom\",\"type\":\"Cat\"},{\"name\":\"Max\",\"type\":\"Cat\"},{\"name\":\"Sam\",\"type\":\"Dog\"},{\"name\":\"Jim\",\"type\":\"Cat\"}]},{\"name\":\"Samantha\",\"gender\":\"Female\",\"age\":40,\"pets\":[{\"name\":\"Tabby\",\"type\":\"Cat\"}]},{\"name\":\"Alice\",\"gender\":\"Female\",\"age\":64,\"pets\":[{\"name\":\"Simba\",\"type\":\"Cat\"},{\"name\":\"Nemo\",\"type\":\"Fish\"}]}]";
            dynamic jsonArray = JArray.Parse(jsonString);
            
            var owners = new CatServiceClientForTesting().ParseResultPublic(jsonArray);
            Assert.AreEqual(6, owners.Length);
            Assert.AreEqual("Male", owners[0].Gender);
            Assert.AreEqual("Simba", owners[5].Cats[0].Name);
        }
    }
}
