﻿namespace CatRetriever
{
    public class Owner
    {
        public string Gender { get; set; }

        public Cat[] Cats { get; set; }
    }
}
